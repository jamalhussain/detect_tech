#!/usr/bin/env python3
import argparse
import cv2
import multiprocessing as mp
import time
import sys, os
import logging
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


parser = argparse.ArgumentParser(description='The following argument support in this program -',)
parser.add_argument("-f",dest='filename',help="Please provide path for input video")
parser.add_argument('-o', dest='operations', metavar='Operation Type', type=str, nargs='+', help='Enter any argument for operation : ["rotate", "gray", "save"]')

args = parser.parse_args()

logger = logging.getLogger(__name__)
logging.basicConfig(filename='output.log',level=logging.INFO,
		format="%(asctime)s:%(levelname)s:%(message)s")


input_path = 'input'
monitor_time = 10
if not os.path.exists(input_path):
	os.makedirs(input_path)




def progress_bar(count, total, fp, suffix='Completed'):
    bar_len = 20
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '#' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('Progress [%s] %s%s ...%s, FPS : %s\r' % (bar, int(percents), '%', suffix,fp))

    sys.stdout.flush()

def operation_type(arg,img,videowrite):
	if (arg=="rotate"):
		img = cv2.rotate(img,cv2.ROTATE_90_CLOCKWISE)
	elif(arg=="gray"):
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		
	elif(arg=="save"):
		videowrite.write(img)
	return img
	


cap = None

if (args.filename != None and args.filename.split(".")[-1] in ["mp4","avi"]):
	#vid_ext = args.filename.split(".")[-1]
	cap = cv2.VideoCapture(str(args.filename))
	total_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
	fps = int(cap.get(cv2.CAP_PROP_FPS))

def frame_setup(frame_cap,width,height,fp,color,input_filename):
	frame_width = int(frame_cap.get(width)) 
	frame_height = int(frame_cap.get(height)) 
	size = (frame_width,frame_height)
	fourcc = cv2.VideoWriter_fourcc(*'mp4v')
	output_path = 'output'
	if not os.path.exists(output_path):
		os.makedirs(output_path)
	if(sys.platform=='linux'):
		result = cv2.VideoWriter(output_path+'/output_'+str(input_filename), fourcc, fp, size,color)
	elif(sys.platform=='win32'):
		result = cv2.VideoWriter(output_path+'\\output_'+str(input_filename), fourcc, fp, size,color)
	return result

	
def video_frame(video_capture,input_video=args.filename,args_parse=["save"]):
	start = time.time()
	k = 0
	input_video = input_video.split("/")[-1]
	total_frame = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT))
	fps = int(video_capture.get(cv2.CAP_PROP_FPS))
	print('\n############# Video Information #################')
	logger.info('# Video Length(Min:sec) : %s:%s, \n# FPS = %s, \n# Frame Size(WxH) : %sx%s'%(int((total_frame/fps)/60),int((total_frame/fps)%60), fps,int(video_capture.get(3)),int(video_capture.get(4))))
	print('# Video Length(Min:sec) : %s:%s, \n# FPS = %s, \n# Frame Size(WxH) : %sx%s'%(int((total_frame/fps)/60),int((total_frame/fps)%60), fps,int(video_capture.get(3)),int(video_capture.get(4))))
	print('#################################################')
	result = None
	if ("save" in args_parse):
		args_parse.append(args_parse.pop(args_parse.index("save")))
		result = frame_setup(video_capture,3,4,fps,1,input_video)
		if ("rotate" in args_parse and "gray" in args_parse):
			result = frame_setup(video_capture,4,3,fps,0,input_video)
			logger.info("Operation Type : Rotate, Gray Scale and Save")
			print("Operation Type : Rotate, Gray Scale and Save")
		elif ("rotate" in args_parse):
			result = frame_setup(video_capture,4,3,fps,1,input_video)
			logger.info("Operation Type : Rotate and Save")
			print("Operation Type : Rotate and Save")
		elif ("gray" in args_parse):
			result = frame_setup(video_capture,3,4,fps,0,input_video)
			logger.info("Operation Type : Gray Scale and Save")
			print("Operation Type : Gray Scale and Save")
		else:
			logger.info('No other operation has been selected. Video is saving without any change')
	while True:
		timer = cv2.getTickCount()
		ret, img = video_capture.read()
		k +=1
		if not ret:
			break
		fps = cv2.getTickFrequency()/(cv2.getTickCount() - timer)
		progress_bar(k, total_frame,int(fps))
		for operation in args_parse:
			img = operation_type(operation,img,result)
		if ("show" in args_parse):
			cv2.imshow("Frame",img)
			key = cv2.waitKey(1) & 0xFF 
			if key ==  ord('q'):
				break
			
	print('\n')
	video_capture.release()
	if ("save" in args_parse):
		result.release()
	cv2.destroyAllWindows()
	end = time.time()
	print('Process time : %.2f sec'%(float(end-start)),'\n############## End of operation ###################')
	logger.info('Process time : %.2f sec, FPS: %s'%(float(end-start) , int(fps)))

class MyHandler(FileSystemEventHandler):
    def on_created(self, event):
    	ext = event.src_path.split(".")[1]
    	if (event.event_type=='created' and ext in ["mp4","avi"]):
    		if(sys.platform=='linux'):
	    		newfile = event.src_path.split("/")
    		elif(sys.platform=='win32'):
	    		newfile = event.src_path.split("\\")
    		logger.info('\nFound New file %s in %s directory. \nPerforming %s operations. '%(newfile[-1],newfile[:-1],args.operations))
    		print('\nFound New file %s in %s directory. \nPerforming %s operations. '%(newfile[-1],newfile[:-1],args.operations))
    		time.sleep(3)
    		cap = cv2.VideoCapture(str(event.src_path))
    		videosave = video_frame(cap,newfile[1],args.operations)
    	return event.src_path



if __name__=="__main__":
	if (args.operations != None):
		try:
			videosave = video_frame(cap,args.filename,args.operations)
		except:
			exc_type, exc_value, exc_traceback = sys.exc_info()
			traceback_details = {'filename': exc_traceback.tb_frame.f_code.co_filename,
			'lineno'  : exc_traceback.tb_lineno,
			'name'    : exc_traceback.tb_frame.f_code.co_name,
			'type'    : exc_type.__name__,'message' : str(exc_value)}
			logger.info('Error ouccerd in Video input, line: %s, type: %s, message: %s'%(traceback_details['lineno'],traceback_details['type'],traceback_details['message']))
			print('Waiting for new file in input directory...')
		else:
			pass
		
		event_handler = MyHandler()
		observer = Observer()
		observer.schedule(event_handler, path=input_path, recursive=True)
		observer.start()
		ct = time.time()+(monitor_time*60)
		while (time.time()<=ct):
			time.sleep(1)
		    
		observer.stop()
		observer.join()
		print('End of program')
		
