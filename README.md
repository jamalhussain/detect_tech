# **#########  CV Operations Problem Statement - 2  #########**

## Description : 
The objective of the following python package(main.py) is to perform some operation on the user input video as written below -
- Rotate each frame by 90 degrees 
- Convert the frame to grayscale
- Save the output as a new video


To make the program robust following points have been considered:
1. Modularity
2. Packaging
3. Scalability
4. Reproducibility

## Setup:
Before running the program make sure you have completed the setup process-
- [x] install the required dependencies from requirments.txt -
    ```sh
    $ pip install -r requirements.txt
    ```
- (optional) Create input directory in the present working dir (otherwise the system will create it by itself )
## 1. Modularity:
We used the command line argument to take the video input from command line and also the type of operation as an optional input parameter. In addition to that, shebang has also used to give it more modularity flavour.
- To check the different argument use the following line in the terminal -
    
    ```sh
    $ ./main.py -h
    ```
- you will get a message :
     ```sh
    optional arguments:
    -h, --help 	for help
    -f FILENAME	takes path for video input, eg. test.mp4
    -o OPERATION	takes multiple inputs from the user to perform the specific operation such as rotate, gray, save, show.
    ``` 
- How to use the command line argument: Go to terminal and write -
    ```sh
    $ ./main.py -f test.mp4 -o rotate gray save
    ``` 
- Note: argument position and order is independent, you can use any argument after ./main.py and you can also select any one of the operations as per need, eg.
 
    ```sh
    $ ./main.py -o save gray -f test.mp4
    $ ./main.py -f test.mp4 -o rotate save
    ``` 
- **our program is very robust. If you forget to provide the path for video input, the program will still run, and it will wait for any new video inside the input folder. All the output file will be named as "output_{inputVideo}.mp4" and saved inside the new directory "output", which will create automatically when you select save argument in the command line.**


## 2. Packaging
(a). You can use main.py program as remodule and can be import to other python program to perform the operation as mentioned in line 5-7. Let's see how to use this module in other python programs -
```python
import main
import cv2
path = "test.mp4"
cap = cv2.VideoCapture(path)
main.video_frame(cap,path,["rotate","gray","save"])
```

##### NOTE: 
- output video will be named as "output_{inputVideo}.mp4" and saved inside the "output" directory.
- Remember, while importing main.py as a module you don't have to provide the command line argument,  command line argument will only work when you are executing the main.py directly.
- video_frame(agr1,arg2,arg3), takes three argument. arg1 = videoCapture, arg2 = path, arg3 = list of operation.

(b) Program can also monitor the specific dir(let's say input dir.) for any new video file for specific time. If there is any new video pasted inside the input dir, program will automatically detect it and performs the same operation given in command line argument.
- As you execute the main.py, program will start monitoring the input dir. 
- If you have given video input by command line, program will operate first to the given input file and then it will select the video file from input dir. 
- If you have not provided any video input from command line argument so program will still run and it will monitor the input dir for new video file. eg.
    ```sh
    $ ./main.py -o rotate gray save 
    ``` 
- To run the program in background(linux os) continiously with no hangup, you can use the folowing command, 
   ```sh
    $ nohup python3 ./main.py -o rotate gray save 
    ``` 
- one can check the background process :
    ```sh
    $ tail -f nohup.out
    ``` 
- There is no need to wait for pasting the new videos inside the input directory if any operation is already in progress. You can paste any number of videos at the same time, the program will perform the operation to all the videos.
- It will not re-process any already processed videos.
- Note: Monitoring function will only work if you are directly executing the main.py.

(c ) Logging: All the info debug data will be logged in the separate logfile "output.log". 

## 3. Scalability
(a) To measure the performance FPS and mean FPS will also log inside the output.log and in terminal.
(b) Multiprocessing: 
## 4. Reproducibility
(a) All the file has been uploaded in GitLab. It can be clone by using the given link :  [https://gitlab.com/jamalhussain/detect.git](https://gitlab.com/jamalhussain/detect_tech.git)

